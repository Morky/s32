// Get Dependencies
const express = require('express');
const mongoose = require('mongoose');
const productRoute = require('./routes/productRoute');

// Set setup
const app = express();
const port = 3001;
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Connecto MongoDB
mongoose.connect('mongodb+srv://admin:admin1234@zuitt-bootcamp.qrju6.mongodb.net/REST-API-mini-activity?retryWrites=true&w=majority',{
	useNewUrlParser: true,
	useUnifiedTopology: true
})

// Allows all the product routes created in the 'productRoute.js' file to use '/products' route
app.use('/products',productRoute);



// Server listening
app.listen(port,()=>console.log(`Listening to port ${port}`));