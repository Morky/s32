const Product = require('../models/product');

// Create new product
/*module.exports.createProduct = (getFromProductRoute)=>{
	let newProduct = new Product({
		name: getFromProductRoute.name,
		price: getFromProductRoute.price
	})
	return newProduct.save().then((product,error)=>{
		if (error) {
			console.log(error);
		} else {
			return product;
		}
	})
}*/

module.exports.createProduct = (getFromProductRoute)=>{
	return Product.findOne({name: getFromProductRoute.body.name}).then((result, error)=>{
		if (result.name == getFromProductRoute.body.name) {
			return 'Duplicate!'
		} else {
			return 'No Duplicate!'
		}
	})

}

// Retrieve all products
module.exports.getAllProducts = ()=>{
	return Product.find({}).then(result=>{
		return result;
	})
}

// Retrieve product where id=id
module.exports.getProduct = (productId)=>{
	return Product.findById(productId).then((result,error)=>{
		if (error) {
			console.log(error);
			return false;
		}else{
			return result;
		}
	})
}

// Delete product where id=id
module.exports.deleteProduct = (productId)=>{
	return Product.findByIdAndRemove(productId).then((removedProduct, error)=>{
		if (error) {
			console.log(error);
			return false;
		} else {
			return removedProduct;
		}
	})
}