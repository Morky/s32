// Create Schema and Model
const mongoose = require('mongoose');
const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: true,
		max: 100
	},
	price: {
		type: Number,
		required: true
	},
})

module.exports = mongoose.model('Product',productSchema);