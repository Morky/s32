const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController');


// Route to create new product
router.post('/create',(req,res)=>{
	productController.createProduct(req.body).then(resultFromController => res.send(resultFromController));
})

// Route to retrieve all products
router.get('/',(req,res)=>{
	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
})

// Route to retrieve product where id=id
router.get('/:id',(req,res)=>{
	productController.getProduct(req.params.id).then(resultFromController => res.send(resultFromController));
})

// Route to delete product where id=id
router.delete('/delete/:id',(req,res)=>{
	productController.deleteProduct(req.params.id).then(resultFromController => res.send(resultFromController));
})

module.exports = router;